# A Simple State Management for React

## Why

* Shared State
* Pass values to deep components
* Decoupling
  * Components
  * Separation of Concerns
  * Testability
