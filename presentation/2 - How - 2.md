# A Simple State Management for React

## How (2)

#### Anathomy of refactored code:

1. [x] A component without state management logic
2. [x] A separated class which handles state management logic
3. [ ] A HOC that connects them together

#### How does the connector should look like? (Redux!)

``` jsx
const Counter = ({ counter, started, start, pause }) => <div> blah blah </div>

const mapStoresToProps = ({ counterStore }) => (
  {
    counter: myStore.counter,
    started: myStore.started,
    start: myStore.start,
    pause: myStore.stop
  }
);

export default connect(mapStoresToProps)(Counter);
```
