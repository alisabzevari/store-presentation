# A Simple State Management for React

## How (1)

#### Let's extract state management logic from this component:

```jsx
class Counter extends React.Component {
  constructor() {
    this.state = {
      counter: 0,
      started: false
    }

    this.start = this.start.bind(this);
    this.pause = this.pause.bind(this);
  }

  start() {
    const { counter } = this.state;

    this.setState({ started: true });
    this.timer = setInterval(() => {
      this.setState({ counter: counter + 1 });
    }, 1000);
  }

  pause() {
    clearInterval(this.timer);
    this.setState({ started: false });
  }

  render() {
    const { started, count } = this.state;
    <div>
      {counter}
      <button onClick={() => started ? this.pause() : this.start()}>
        {started ? 'Stop' : 'Start'}
      </button>
    </div>
  }
}
```

