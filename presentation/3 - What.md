# A Simple State Management for React

## What

#### The API:

``` js
import {
  Store,
  register,
  connect
} from './Store';
```

#### Create a store:

``` js
import { Store } from '../store';

export default class CounterStore extends Store {
  constructor() {
    super();

    this.state = {
      counter: 0,
      started: false
    }

    this.start = this.start.bind(this);
    this.pause = this.pause.bind(this);
  }

  start() {
    this.setState({ started: true });
    this.timer = setInterval(() => {
      this.setState({ counter: this.state.counter + 1 });
    }, 1000);
  }

  pause() {
    clearInterval(this.timer);
    this.setState({ started: false });
  }

  get counter() {
    return this.state.counter;
  }

  get started() {
    return this.state.started;
  }
}
```

#### Bind a store to Dependency Container:

``` js
import { register } from '../store';

import CounterStore from './CounterStore';

const counterStore = new CounterStore();

register({ counterStore });
```

#### connect store to component:

``` jsx
import React from 'react';

import { connect } from '../store';

const Counter = ({ start, counter, started, pause }) => (
  <div>
    {counter}
    <button onClick={() => started ? pause() : start()}>{started ? 'Stop' : 'Start'}</button>
  </div>
);
Counter.displayName = 'Counter';

const mapStoresToProps = ({ counterStore }) => (
  {
    start: counterStore.start,
    counter: counterStore.counter,
    started: counterStore.started,
    pause: counterStore.pause
  }
);

export default connect(mapStoresToProps)(Counter);
```

Done!
