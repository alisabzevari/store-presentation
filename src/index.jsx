import React from 'react';
import { render } from 'react-dom';

import registerStores from './modules';
import App from './components/App';

registerStores();

render(
  <App />,
  document.getElementById('root')
);

