import { Store } from '../store';

export default class GithubReposStore extends Store {
  constructor() {
    super();

    this.state = {
      username: 'facebook',
      repos: [],
      isLoading: false,
      error: null
    };

    this.fetchRepos = this.fetchRepos.bind(this);
  }

  fetchRepos(username) {
    this.setState({ isLoading: true, username, error: null });
    fetch(`https://api.github.com/users/${username}/repos`)
      .then(response => {
        if (!response.ok) {
          throw new Error(response.statusText);
        }
        return response.json();
      })
      .then(result => {
        this.setState({ repos: result, isLoading: false });
      })
      .catch(error => {
        this.setState({ error, isLoading: false })
      });
  }

  get username() {
    return this.state.username;
  }

  get repos() {
    return this.state.repos;
  }

  get isLoading() {
    return this.state.isLoading;
  }

  get error() {
    return this.state.error;
  }
}
