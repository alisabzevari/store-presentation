import { Store } from '../store';

export default class CounterStore extends Store {
  constructor() {
    super();

    this.state = {
      counter: 0,
      started: false
    }

    this.start = this.start.bind(this);
    this.pause = this.pause.bind(this);
  }

  start() {
    this.setState({ started: true });
    this.timer = setInterval(() => {
      this.setState({ counter: this.state.counter + 1 });
    }, 1000);
  }

  pause() {
    clearInterval(this.timer);
    this.setState({ started: false });
  }

  get counter() {
    return this.state.counter;
  }

  get started() {
    return this.state.started;
  }
}
