import { register } from '../store';

import GithubReposStore from './GithubReposStore';
import CounterStore from './CounterStore';

const registerStores = () => {
  const githubReposStore = new GithubReposStore();
  const counterStore = new CounterStore();

  const stores = {
    githubReposStore,
    counterStore
  };

  if (process.env.NODE_ENV === 'development') {
    window.__STORES__ = stores; // eslint-disable-line no-underscore-dangle
  }

  register(stores);
};

export default registerStores;
