import * as React from 'react';
import { connect } from '../store';
import GithubReposStore from '../modules/GithubReposStore'

interface GithubReposProps {
  fetchRepos: (username: string) => void;
  repos: { name: string }[];
  isLoading: boolean;
  error: Error;
}

interface GithubReposState {
  username: string;
}

class GithubRepos extends React.Component<GithubReposProps, GithubReposState> {
  constructor(props) {
    super(props);

    this.state = {
      username: props.username || ''
    };

    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handleOkClick = this.handleOkClick.bind(this);
  }

  handleUsernameChange(event) {
    this.setState({ username: event.target.value });
  }

  handleOkClick() {
    this.props.fetchRepos(this.state.username);
  }

  renderResult() {
    const { repos, isLoading, error } = this.props;
    
    if (isLoading) {
      return 'Loading...'
    }

    return error
      ? <div> {error.message} </div>
      : <ul> {repos.map(repo => <li key={repo.name}>{repo.name}</li>)} </ul>
  }

  render() {
    return (
      <div>
        <input type="text" value={this.state.username} onChange={this.handleUsernameChange} />
        <button onClick={this.handleOkClick}>OK</button>
        <br />
        {this.renderResult()}
      </div>
    );
  }
}

const mapStoresToProps = ({ githubReposStore }: { githubReposStore: GithubReposStore }) => (
  {
    username: githubReposStore.username,
    isLoading: githubReposStore.isLoading,
    error: githubReposStore.error,
    repos: githubReposStore.repos,
    fetchRepos: githubReposStore.fetchRepos,
  }
);

export default connect(mapStoresToProps)(GithubRepos);
