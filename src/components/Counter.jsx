import React from 'react';

import { connect } from '../store';

const Counter = ({ start, counter, started, pause }) => (
  <div>
    {counter}
    <button onClick={() => started ? pause() : start()}>{started ? 'Stop' : 'Start'}</button>
  </div>
);
Counter.displayName = 'Counter';

const mapStoresToProps = ({ counterStore }) => (
  {
    start: counterStore.start,
    counter: counterStore.counter,
    started: counterStore.started,
    pause: counterStore.pause
  }
);

export default connect(mapStoresToProps)(Counter);
