import React from 'react';
import Counter from './Counter';
import GithubRepos from './GithubRepos';

const App = () => (
  <div>
    <Counter />
    <GithubRepos />
  </div>
);
App.displayName = 'App';

export default App;
